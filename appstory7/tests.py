from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_todo
from .models import Todo
from .forms import Todo_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.


class Story6UnitTest(TestCase):

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Todo.objects.create(
            title='mengerjakan lab ppw', description='mengerjakan story_7 ppw')
# Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Todo_Form()
        self.assertIn('class="todo-form-input', form.as_p())
        self.assertIn('id="id_title"', form.as_p())
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

class Story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/story7ies/')
        # find the form element
        title = selenium.find_element_by_id('id_title')
        description = selenium.find_element_by_id('id_description')
        time.sleep(5)
        submit = selenium.find_element_by_id('submit')
        input_string = "COBACOBA"
        # Fill the form with data
        title.send_keys('Mengerjakan Lab PPW')
        description.send_keys('Story 6 with Functional Test')

        # submitting the form
        submit.send_keys(Keys.RETURN)