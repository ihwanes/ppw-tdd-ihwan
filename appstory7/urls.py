from django.conf.urls import url
from .views import index

appname = 'appstory7'
urlpatterns = [
  url(r'^$', index, name='index'),
]
